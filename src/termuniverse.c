/*
 *   This file is part of TermUniverse
 *   Copyright 2011-2015 JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>


int main()
{
  int key;
  int cont, tick=0, contshots;

  int maxx,maxy;
  int starx[300], stary[300];

  // Player vars
  struct
  { int x, y;
    int shotx[60], shoty[60];
    char lives;
    int life, shield, ammo, charges;
  } player;

  char fighter[5][8] = { "#=#>-",		// Images for player ship
			 "#//> ",
			 "[=(= ",
			 ")>>= ",
			 "]=}> "
                        };
  char selfighter = 0; // Selected fighter for player


  // Enemies vars
  struct
  { int x, y;
    int shotx[60], shoty[60];
    char type;
    int life, shield, ammo;
  } enemy[50];

  char eships[12][8] = { "<##  ",		// Images for enemy ships
			 "<+#  ",
			 "=<(= ",
			 "=<<( ",
			 "=<@( ",
			 "=<|( ",
			 "=<-( ",
			 "<{=}["
                        };


  //////////////////////////////////////////////////// Start up
  printf("Term Universe v0.1\n");
  sleep(1);


  initscr();  // Init Ncurses!
  noecho();
  keypad(stdscr, true);
  nodelay(stdscr, true);
  //halfdelay(1);
  //raw();
  curs_set(0);
  getmaxyx(stdscr,maxy, maxx);
  start_color();

  // Color pair definitions!
  init_pair(1, COLOR_RED,   COLOR_BLACK);
  init_pair(2, COLOR_GREEN, COLOR_BLACK);
  init_pair(3, COLOR_BLUE,  COLOR_BLACK);
  init_pair(4, COLOR_WHITE, COLOR_BLACK);
  init_pair(5, COLOR_YELLOW, COLOR_BLACK);


  // Init stars!
  srand(100);
  for (cont=0; cont!=200; ++cont)
  {
      starx[cont] = rand() / 10000000;
      if (starx[cont] > maxx)
      {
          starx[cont] /= 2;
      }

      stary[cont] = rand() / 20000000;
      if (stary[cont] > maxy)
      {
          stary[cont] /= 2;
      }

      // printw("x: %d  -- y: %d\n", starx[cont], stary[cont]);      usleep(9000);      refresh();
  }



  key=0;


  // Init player
  player.lives = 3;
  player.life = 1000;
  player.shield = 1000;
  player.x = 5;
  player.y = maxy / 2;


  // Init shots!!
  player.ammo = 1000;
  for (cont=0; cont!=60; ++cont)
  {
      player.shotx[cont] = -1;
      player.shoty[cont] = -1;
  }



  // Init enemies!
  for (cont=0; cont!=50; cont++)   // very very tmp
  {
      enemy[cont].x = -100;
      enemy[cont].y = 0;
      enemy[cont].type = cont;
      enemy[cont].life = 50;
  }



//////////////////////////////////////////////////////////// Game loop start

  while (key != 27)  // ESC
  {
      werase(stdscr);
      if (tick == 0)
      {
          tick = 1;
      }
      else
      {
          tick=0;
      }


    // Move stars!
    for (cont=0; cont!=100; cont++)
    {
        if (cont & 1)
        {
            attron(A_BOLD | COLOR_PAIR(4));
            mvprintw(stary[cont], starx[cont], ".");
            starx[cont]--;  // always advance
	}
      else
        {
            attroff(A_BOLD);
            attron(COLOR_PAIR(4));
            mvprintw(stary[cont], starx[cont], ".");
            if (tick == 1)
                starx[cont]--;  // advance 1 out of every 2 times
	}
      if (starx[cont] < 0)
      {
          starx[cont]=maxx;
      }
    }


    /////////////////////////////////////// Print enemies, process enemy movement
    attron(A_BOLD | COLOR_PAIR(5));
    for (cont=0; cont!=8; cont++)  // tmp
    { if (enemy[cont].life > 0)
       { if (enemy[cont].x < maxx-4)
          { mvprintw(enemy[cont].y, enemy[cont].x, eships[enemy[cont].type]);
          }
         for (contshots=0; contshots!=60; contshots++)
          { if (enemy[cont].x <= player.shotx[contshots] && enemy[cont].y == player.shoty[contshots]) // enemy hit?
             { enemy[cont].life=0;
	       break;
             }
          }
         enemy[cont].x--;
         if (enemy[cont].x < 0)
          { enemy[cont].x = maxx + (rand()/40000000);
	    enemy[cont].y = 2 + (rand()/40000000);
	  }
       }
    }

    attron(A_BOLD | COLOR_PAIR(3));
    mvprintw(player.y, player.x, fighter[selfighter]);  ////// Print our fighter!!

    attron(A_BOLD | COLOR_PAIR(2));
    for (cont=0; cont!=60; cont++)      //////////////////////// and the shots!
    { if (player.shotx[cont] > -1)
      { mvprintw(player.shoty[cont], player.shotx[cont], "=");
        player.shotx[cont]+=1;
        if (player.shotx[cont] > maxx) player.shotx[cont] = -1; // shot got out, disable!
      }
    }


    attron(A_BOLD | COLOR_PAIR(1));   // Print gauges, meters, etc.
    mvprintw(0, 2, "Fighter: ########--");  // Upper
    mvprintw(0, 22, "Shields: #####-----");  // Upper
    mvprintw(0, 44, "Ammo: %4d", player.ammo);  // Upper

    mvprintw(maxy-1,2, "Key: %3d -- Res: %dx%d -- p.x: %3d, p.y: %3d", key, maxx, maxy, player.x, player.y); //Lower



    usleep(20000);
    // usleep(45000); // under X

    key = getch();
    refresh();

    switch(key)
    {	case 258: // Down
		if (player.y < maxy-2) player.y++;
		break;

	case 259: // Up
        	if (player.y > 1) player.y--;
		break;

	case 260: // Left
		if (player.x > 0) player.x--;
		break;

	case 261: // Right
		if (player.x < maxx-5) player.x++;
		break;

	case 330: // Del, FIRE!!
		if (player.ammo > 0 && tick==0)
		{
			for (cont=0; cont!=60;cont++)
                		{
                		  if (player.shotx[cont] == -1)
                		    {  break;
                		    }
                		}
			player.shotx[cont]=player.x+6;  // Found unused "shot slot", now FIRE!
			player.shoty[cont]=player.y;
			player.ammo--;
		}
		break;

	case 43: // KP+
		if (selfighter<4) selfighter++;
		break;
	case 45: // KP-
		if (selfighter>0) selfighter--;
		break;

    }

  }


//////////////////// end game loop


  werase(stdscr);
  endwin();  // De-init Ncurses!

  printf("\nGame finished.\n\nThanks for playing!\n\n");


  return 0;
}
