 TermUniverse - Space shooter for the terminal
 Copyright 2011-2015 JanKusanagi <jancoding@gmx.com>

==============================================================================

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the
   Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .

==============================================================================


TermUniverse is a space shooter in the style of the very old classics, like R-type.
It runs on a terminal (preferably a pure tty), and is based on GNU Ncurses.



Compiling:

You can use qmake:
  From TermUniverse main directory, where TermUniverse.pro is located:

        mkdir build		# create a clean directory for the build
        cd build		# go into it
        qmake ..		# ask Qmake to generate a Makefile
        make			# run Make to compile the project


Or you can compile it directly with a simple call to gcc:
  gcc src/termuniverse.c -o termuniverse -lcurses


That should do it.


Dependencies:
You'll need the ncurses-devel packages:
  - Debian: ncurses-dev
  - Mageia: libncurses-devel


Visit http://jancoding.wordpress.com for more information.
Get the latest source from https://gitlab.com/termuniverse/termuniverse-dev
